Infostrates Ibexa Content Seo
===============================

A bundle to handle SEO part of an Ibexa Content project.

It handles :

- Automatic meta values (including title)
- Admin UI tab on any content render in a full view to manually set any meta value.
- Automatic href lang based on content available language.

Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
$ composer config repositories.ibexa_content_seo vcs https://bitbucket.org/Infostrates/ibexa-content-seo.git
$ composer require infostrates/ibexa-content-seo
```

### Step 2: Only for applications that don't use Symfony Flex, enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Infostrates\IbexaContentSeo\InfostratesIbexaContentSeoBundle::class => ['all' => true],
];
```

### Step 3: Update your database schema

Update database schema accordingly to your project. Here's an example using tanoconsulting/ezmigrationbundle2 bundle

- Run the following command :

```bash
php bin/console doctrine:schema:update --dump-sql
```

- Copy the output to a new sql migration file in your migrations folder.
- Run your migration :

```bash
php bin/console bin/console kaliop:migration:migrate
```

### Step 4: adjust your admin ui settings

To get correct values in admin ui for the automatic canonical value, you have to put all front siteaccess as related to
admin_group. For example :

```yaml
parameters:
    ezsettings.admin_group.related_siteaccesses: [ site_fr, site_en ]
```

### Step 5: add policy to adequate roles

You may want to add the "Seo > edit" policy to any suitable role, to allow user to see and interact with the SEO tab in
Admin UI's content view.

### Step 6: add seo_metas function to your layout template

Place the following line in your layout template, and remove any meta found
in ```infostrates_ibexa_content_seo.meta_properties``` parameter :

```twig
{{ seo_metas() }}
```

## Overriding an automatic value

Any automatic meta value can be overriden by setting its value in its key in the twig variable ```forced_metas```.

Example to force the title:

```twig
{% set forced_metas = {'title': 'Forced automatic title'} %}
```

Note that it won't be applied if a manual value has been inputted in Admin UI : the manual value is always prioritized.

## Visibility of the tab

The SEO tab is set to be visible only on pertinent content using
\Infostrates\IbexaContentSeo\Domains\TabVisibilityManagerInterface.
If the tab does not appear as expected, simply implement your own logic by replacing the default implementation in
infostrates_ibexa_content_seo.tab_visibility.

## Configuration reference

See [default_config.yaml](config/default_config.yaml) for details.

## Contributing to this bundle

No dev stack for now, you'll have to install php on your workspace. CI tests can be run locally with :

```
make test
```

Versions are made simply by tagging the desired commit (don't forget to push the tags !).

## TODO

- Sitemap feature (for now, use [presta/sitemap-bundle](https://github.com/prestaconcept/PrestaSitemapBundle), with an
  implementation example
  here : https://bitbucket.org/infostrates/ly-01-vacances-bleues-refonte-site/src/f3086d4b373d3c0f807d1b11b226bbde99c2b63d/src/Ui/Sitemap.php#Sitemap.php)
- Translations for the Admin UI tab interface
