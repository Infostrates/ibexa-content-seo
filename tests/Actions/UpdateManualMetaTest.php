<?php

namespace Infostrates\IbexaContentSeo\Tests\Actions;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Ibexa\Contracts\Core\Repository\ContentService;
use Ibexa\Core\MVC\Symfony\SiteAccess;
use Ibexa\AdminUi\Tab\LocationView\ContentTab;
use Infostrates\IbexaContentSeo\Actions\UpdateManualMeta;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Entity;
use Infostrates\IbexaContentSeo\Tests\Domains\Meta\ManualValue\RepositoryTest;
use Infostrates\IbexaContentSeo\Tests\Helpers\EzContentStub;
use PHPUnit\Framework\MockObject\Stub;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class UpdateManualMetaTest extends KernelTestCase
{
    /**
     * @var ContentService&Stub
     */
    private $contentServiceStub;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->contentServiceStub = $this->createStub(ContentService::class);
        static::getContainer()->set('ezpublish.api.service.content', $this->contentServiceStub);
    }

    public function testInvoke(): void
    {
        /** @var UpdateManualMeta $action */
        $action = static::getContainer()->get('infostrates_ibexa_content_seo.actions.update_manual_meta');

        /** @var Registry $registry */
        $registry = self::getContainer()->get('doctrine');
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $registry->getManager();
        RepositoryTest::truncateManualMetaTable($entityManager);

        $request = $this->getAdminRequest();
        $request->setMethod('POST');
        $request->request->set('content_meta_form', ['title' => 'test']);

        $content = EzContentStub::getTestFakeContent();
        $this->contentServiceStub->method('loadContent')->willReturn($content);
        $response = ($action)($request, $content->id, 'fre-FR');
        $this->assertCount(1, $entityManager->getRepository(Entity::class)->findAll());
        $this->assertInstanceOf(RedirectResponse::class, $response);
    }

    private function getAdminRequest(): Request
    {
        $request = Request::create(
            '/admin/view/content/42/full/1/44#ez-tab-location-view-seo#tab',
            Request::METHOD_GET,
        );

        $request->attributes->set('siteaccess', new SiteAccess('admin'));

        return $request;
    }
}
