<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Tests\Helpers;

use Twig\Environment;
use Twig\TwigFunction;

final class TwigFunctionsStub
{
    public static function applyFunctionsStub(Environment $twig): void
    {
        $twig->registerUndefinedFunctionCallback(function ($name) {
            if ($name === 'path') {
                return new TwigFunction('path', fn($route) => ('/route_' . $route));
            }

            return false;
        });
    }
}
