<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Tests\Helpers;

use Ibexa\Contracts\Core\Repository\Values\Content\ContentInfo;
use Ibexa\Core\Repository\Values\Content\Content;
use Ibexa\Core\Repository\Values\Content\Location;
use Ibexa\Core\Repository\Values\Content\VersionInfo;
use Ibexa\Core\Repository\Values\ContentType\ContentType;

final class EzContentStub
{
    public static function getTestFakeContent(?string $contentTypeIdentifier = null): Content
    {
        $contentInfo = self::getTestFakeContentInfo();
        $versionInfo = new VersionInfo(['contentInfo' => $contentInfo, 'languageCodes' => ['fre-FR', 'eng-GB']]);

        $contentType = null;
        if ($contentTypeIdentifier) {
            $contentType = new ContentType(['identifier' => $contentTypeIdentifier]);
        }

        return new Content(['versionInfo' => $versionInfo, 'contentType' => $contentType]);
    }

    public static function getTestFakeLocation(): Location
    {
        return new Location([
            'contentInfo' => self::getTestFakeContentInfo(),
        ]);
    }

    private static function getTestFakeContentInfo(): ContentInfo
    {
        return new ContentInfo(['id' => 42, 'mainLanguageCode' => 'fre-FR']);
    }
}
