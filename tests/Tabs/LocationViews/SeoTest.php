<?php

namespace Infostrates\IbexaContentSeo\Tests\Tabs\LocationViews;

use DomainException;
use Ibexa\Contracts\Core\Repository\PermissionResolver;
use Ibexa\Contracts\Core\Repository\Values\Content\Language;
use Ibexa\Core\MVC\Symfony\SiteAccess;
use Ibexa\Core\Repository\Values\Content\Content;
use Ibexa\Core\Repository\Values\ContentType\ContentType;
use Ibexa\AdminUi\Tab\LocationView\ContentTab;
use Infostrates\IbexaContentSeo\Tabs\LocationViews\Seo;
use Infostrates\IbexaContentSeo\Tests\Helpers\EzContentStub;
use Infostrates\IbexaContentSeo\Tests\Helpers\TwigFunctionsStub;
use Infostrates\IbexaContentUtils\PageContentTypeGuesser;
use PHPUnit\Framework\MockObject\Stub;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class SeoTest extends KernelTestCase
{
    /** @var ContentTab&Stub  */
    private ContentTab $contentTabStub;
    /** @var PermissionResolver&Stub */
    private PermissionResolver $permissionResolver;
    /** @var PageContentTypeGuesser&Stub */
    private PageContentTypeGuesser $pageContentTypeGuesser;

    protected function setUp(): void
    {
        self::bootKernel();

        $this->contentTabStub = $this->createStub(ContentTab::class);
        static::getContainer()->set(ContentTab::class, $this->contentTabStub);

        $this->permissionResolver = $this->createStub(PermissionResolver::class);
        static::getContainer()->set(PermissionResolver::class, $this->permissionResolver);

        $this->pageContentTypeGuesser = $this->createStub(PageContentTypeGuesser::class);
        static::getContainer()->set(PageContentTypeGuesser::class, $this->pageContentTypeGuesser);

        $environment = static::getContainer()->get('twig');
        if (!$environment instanceof Environment) {
            throw new DomainException('Unexpected environment type');
        }
        TwigFunctionsStub::applyFunctionsStub($environment);
    }

    public function testEvaluate(): void
    {
        $this->permissionResolver->method('canUser')->willReturn(true);
        $this->pageContentTypeGuesser->method('hasContentTypeHaveContentPage')->willReturn(true);
        /** @var Seo $seo */
        $seo = static::getContainer()->get(Seo::class);

        $this->assertFalse($seo->evaluate([]));
        $this->assertFalse($seo->evaluate(['contentType' => new ContentType()]));
        $this->assertFalse($seo->evaluate(['content' => new Content()]));

        $this->assertTrue($seo->evaluate(['contentType' => new ContentType(), 'content' => new Content()]));
    }

    public function testEvaluateUnauthorized(): void
    {
        $this->permissionResolver->method('canUser')->willReturn(false);
        $this->pageContentTypeGuesser->method('hasContentTypeHaveContentPage')->willReturn(true);
        /** @var Seo $seo */
        $seo = static::getContainer()->get(Seo::class);

        $this->assertFalse($seo->evaluate(['contentType' => new ContentType(), 'content' => new Content()]));
    }

    public function testEvaluateNonPageContent(): void
    {
        $this->permissionResolver->method('canUser')->willReturn(true);
        $this->pageContentTypeGuesser->method('hasContentTypeHaveContentPage')->willReturn(false);
        /** @var Seo $seo */
        $seo = static::getContainer()->get(Seo::class);

        $this->assertFalse($seo->evaluate(['contentType' => new ContentType(), 'content' => new Content()]));
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testRenderView(): void
    {
        $this->contentTabStub
            ->method('loadContentLanguages')
            ->willReturn([
                new Language(['languageCode' => 'eng-GB', 'name' => 'English']),
                new Language(['languageCode' => 'fre-FR', 'name' => 'Français']),
            ])
        ;

        $environment = static::getContainer()->get('twig');
        if (!$environment instanceof Environment) {
            throw new DomainException('Unexpected environment type');
        }
        $environment->addGlobal('app', ['request' => new Request([], [], ['languageCode' => 'eng-GB'])]);

        /** @var Seo $seo */
        $seo = static::getContainer()->get(Seo::class);
        /** @var RequestStack $requestStack */
        $requestStack = static::getContainer()->get('request_stack');
        $requestStack->push($this->getAdminRequest());

        $this->assertStringContainsString(
            'Apply for language',
            $seo->renderView([
                'content' => EzContentStub::getTestFakeContent(),
                'location' => EzContentStub::getTestFakeLocation(),
            ])
        );

        $this->expectExceptionObject(new DomainException('Unexpected content type'));
        $seo->renderView([]);
    }

    private function getAdminRequest(): Request
    {
        $request = Request::create(
            '/admin/view/content/42/full/1/44#ez-tab-location-view-seo#tab',
            Request::METHOD_GET,
        );

        $request->attributes->set('siteaccess', new SiteAccess('admin'));

        return $request;
    }
}
