<?php

namespace Infostrates\IbexaContentSeo\Tests\Domains\Meta\ManualValue;

use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\Builder;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Entity;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormData;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormDataMapper;
use Infostrates\IbexaContentSeo\Tests\Domains\Meta\MetaPropertyRegistryTest;
use Infostrates\IbexaContentSeo\Tests\Helpers\EzContentStub;
use PHPUnit\Framework\MockObject\Stub;
use PHPUnit\Framework\TestCase;

class FormDataMapperTest extends TestCase
{
    private FormDataMapper $testSubject;
    /** @var Builder&Stub */
    private Builder $automaticValueBuilderStub;

    protected function setUp(): void
    {
        $this->automaticValueBuilderStub = $this->createStub(Builder::class);
        $this->testSubject = new FormDataMapper(
            MetaPropertyRegistryTest::getTestFakeMetaPropertyRegistry(),
            $this->automaticValueBuilderStub
        );
    }

    public function testMapEntityListToFormDataList(): void
    {
        $this->automaticValueBuilderStub->method('getAutomaticValue')->willReturnOnConsecutiveCalls(
            'automated_title',
            null,
            'automated_tag',
            'automated_canonical',
            null
        );

        $testEntityList = $this->getTestEntityList();

        $formDataList = $this->testSubject->mapEntityListToFormDataList(
            $testEntityList,
            EzContentStub::getTestFakeContent(),
            'fre-FR'
        );

        $this->assertEquals(
            $this->getTestFormDataList(),
            $formDataList
        );
    }

    public function testMapFormDataListToEntityList(): void
    {
        $testFormDataList = $this->getTestFormDataList();

        $entityList = $this->testSubject->mapFormDataListToEntityList(
            $testFormDataList,
            EzContentStub::getTestFakeContent(),
            'fre-FR'
        );

        $this->assertEquals(
            array_values($this->getTestEntityList()),
            $entityList
        );
    }

    /**
     * @return Entity[]
     */
    private function getTestEntityList(): array
    {
        $content = EzContentStub::getTestFakeContent();
        $languageCode = 'fre-FR';

        $entity1 = new Entity($content->id, $languageCode, 'title');
        $entity1->value = 'content1';

        $entity2 = new Entity($content->id, $languageCode, 'description');
        $entity2->value = 'content2';

        return [
            'title' => $entity1,
            'description' => $entity2,
        ];
    }

    /**
     * @return FormData[]
     */
    private function getTestFormDataList(): array
    {
        return [
            'title' => new FormData('title', 'content1', 'automated_title'),
            'description' => new FormData('description', 'content2', null),
            'tags' => new FormData('tags', null, 'automated_tag'),
            'canonical' => new FormData('canonical', null, 'automated_canonical'),
            'robots' => new FormData('robots', null, null),
        ];
    }
}
