<?php

namespace Infostrates\IbexaContentSeo\Tests\Domains\Meta\ManualValue;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Entity;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Repository;
use Infostrates\IbexaContentSeo\Tests\Helpers\EzContentStub;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RepositoryTest extends KernelTestCase
{
    public static function truncateManualMetaTable(EntityManagerInterface $entityManager): void
    {
        $entityManager->createQueryBuilder()
            ->delete()
            ->from(Entity::class, 'e')
            ->getQuery()
            ->execute()
        ;
    }

    protected function setUp(): void
    {
        self::bootKernel();

        self::truncateManualMetaTable($this->getEntityManager());
    }

    public function testFindForContent(): void
    {
        $content = EzContentStub::getTestFakeContent();

        $titleEntity = new Entity($content->id, 'fre-FR', 'title');
        $titleEntity->value = 'the_value';

        $entityManager = $this->getEntityManager();
        $entityManager->persist($titleEntity);
        $entityManager->flush();

        $testSubject = self::getContainer()->get('infostrates_ibexa_content_seo.domains.manual_value.repository');
        if (!$testSubject instanceof Repository) {
            throw new DomainException('Unexpected test subject type');
        }
        $actualArray = $testSubject->findByPropertyName($content, 'fre-FR');

        self::assertEquals(
            ['title' => $titleEntity],
            $actualArray
        );
    }

    public function testReplaceForContent(): void
    {
        $content = EzContentStub::getTestFakeContent();

        $languageCode = 'fre-FR';
        $oldTitleEntity = new Entity($content->id, $languageCode, 'title');
        $oldTitleEntity->value = 'initial_value';
        $oldRobotsEntity = new Entity($content->id, $languageCode, 'robots');
        $oldRobotsEntity->value = 'to_be_deleted';

        $entityManager = $this->getEntityManager();
        $entityManager->persist($oldTitleEntity);
        $entityManager->persist($oldRobotsEntity);
        $entityManager->flush();

        $newTitleEntity = new Entity($content->id, $languageCode, 'title');
        $newTitleEntity->value = 'new_value';
        $newDescriptionEntity = new Entity($content->id, $languageCode, 'description');
        $newDescriptionEntity->value = 'description_content';

        /** @var Repository $repository */
        $repository = static::getContainer()->get('infostrates_ibexa_content_seo.domains.manual_value.repository');

        $repository->replaceForContent($content, $languageCode, [$newTitleEntity, $newDescriptionEntity]);

        $this->assertEquals(
            [
                'title' => $newTitleEntity,
                'description' => $newDescriptionEntity,
            ],
            $repository->findByPropertyName($content, $languageCode)
        );
    }

    private function getEntityManager(): EntityManagerInterface
    {
        /** @var Registry $registry */
        $registry = self::getContainer()->get('doctrine');

        $manager = $registry->getManager();
        if (!$manager instanceof EntityManagerInterface) {
            throw new DomainException('Unexpected manager type');
        }

        return $manager;
    }
}
