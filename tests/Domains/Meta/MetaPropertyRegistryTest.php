<?php

namespace Infostrates\IbexaContentSeo\Tests\Domains\Meta;

use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaProperty;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaPropertyRegistry;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MetaPropertyRegistryTest extends TestCase
{
    public static function getTestFakeMetaPropertyRegistry(): MetaPropertyRegistry
    {
        $instance = new self();
        /** @var MockObject&ContainerInterface $mockContainer */
        $mockContainer = $instance->createMock(ContainerInterface::class);
        $mockContainer->method('get')->willReturn($instance->createMock(AutoValueProviderInterface::class));

        return new MetaPropertyRegistry(
            [
                ['name' => 'title', 'auto_value_provider' => 'test'],
                ['name' => 'description'],
                ['name' => 'tags'],
                ['name' => 'canonical'],
                ['name' => 'robots'],
            ],
            $mockContainer
        );
    }

    public function testGetAll(): void
    {
        $testSubject = self::getTestFakeMetaPropertyRegistry();

        self::assertEquals(
            [
                new MetaProperty('title', $this->createMock(AutoValueProviderInterface::class), []),
                new MetaProperty('description', null, []),
                new MetaProperty('tags', null, []),
                new MetaProperty('canonical', null, []),
                new MetaProperty('robots', null, []),
            ],
            $testSubject->getAll()
        );
    }
}
