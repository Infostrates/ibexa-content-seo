<?php

namespace Infostrates\IbexaContentSeo\Tests\Domains\Meta;

use Infostrates\IbexaContentSeo\Domains\Meta\ContentMetaForm;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormData;
use InvalidArgumentException;
use Symfony\Component\Form\PreloadedExtension;
use Symfony\Component\Form\Test\TypeTestCase;

class ContentMetaFormTest extends TypeTestCase
{
    /**
     * @return PreloadedExtension[]
     */
    protected function getExtensions(): array
    {
        $type = new ContentMetaForm();

        return [
            new PreloadedExtension([$type], []),
        ];
    }

    public function testBuildForm(): void
    {
        $titleFormData = new FormData('title', 'test_title', null);
        $form = $this->factory->create(ContentMetaForm::class, [
            'title' => $titleFormData,
            'description' => new FormData('description', null, null),
            'tags' => new FormData('tags', null, null),
            'canonical' => new FormData('canonical', null, null),
            'robots' => new FormData('robots', null, null),
        ]);
        $this->assertCount(5, $form->all());
        $this->assertEquals($titleFormData, $form->get('title')->getData());
        $view = $form->createView();
        $this->assertEquals($titleFormData->value, $view->children['title']->vars['value']);

        $this->expectExceptionObject(new InvalidArgumentException('This form should not be created without data'));
        $this->factory->create(ContentMetaForm::class);
    }
}
