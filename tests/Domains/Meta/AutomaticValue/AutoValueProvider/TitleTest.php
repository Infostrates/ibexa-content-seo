<?php

namespace Infostrates\IbexaContentSeo\Tests\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Core\Helper\TranslationHelper;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider\Title;
use Infostrates\IbexaContentSeo\Tests\Helpers\EzContentStub;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TitleTest extends TestCase
{
    private Title $testSubject;
    /** @var TranslationHelper&MockObject  */
    private TranslationHelper $translationHelperMock;

    protected function setUp(): void
    {
        $this->translationHelperMock = $this->createMock(TranslationHelper::class);
        $this->testSubject = new Title(
            $this->translationHelperMock,
            'Site name',
            ['landing_page'],
        );
    }

    public function testBuildAutoValue(): void
    {
        $this->translationHelperMock->method('getTranslatedContentName')->willReturn('Content name');

        $actualValue = $this->testSubject->buildAutoValue(
            EzContentStub::getTestFakeContent('site_page'),
            'eng-GB',
            null
        );
        $this->assertEquals(
            'Content name - Site name',
            $actualValue
        );

        $actualValue = $this->testSubject->buildAutoValue(
            EzContentStub::getTestFakeContent('landing_page'),
            'eng-GB',
            null
        );
        $this->assertEquals(
            'Site name',
            $actualValue
        );
    }
}
