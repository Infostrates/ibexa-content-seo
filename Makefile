test: phpstan phpunit phpcs
rebuild-db: destroy-db init-db

phpstan:
	php -d memory_limit=-1 vendor/bin/phpstan

phpcs:
	php vendor/bin/phpcs

phpcbf:
	php vendor/bin/phpcbf

phpunit:
	php vendor/bin/phpunit

init-db:
	php tests/bin/console doc:database:create
	php tests/bin/console doc:schema:updat --dump-sql --force

destroy-db:
	php tests/bin/console doc:database:drop --force
