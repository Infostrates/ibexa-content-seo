<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Tabs\LocationViews;

use DomainException;
use Ibexa\Contracts\Core\Repository\Exceptions\BadStateException;
use Ibexa\Contracts\Core\Repository\Exceptions\InvalidArgumentException;
use Ibexa\Contracts\Core\Repository\PermissionResolver;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\Content\Location;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Ibexa\Contracts\AdminUi\Tab\ConditionalTabInterface;
use Ibexa\AdminUi\Tab\LocationView\ContentTab;
use Ibexa\Contracts\AdminUi\Tab\TabInterface;
use Infostrates\IbexaContentSeo\Domains\Meta\ContentMetaForm;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormDataMapper;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Repository;
use Infostrates\IbexaContentSeo\Domains\TabVisibilityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class Seo implements TabInterface, ConditionalTabInterface
{
    private Environment $templating;
    private ContentTab $contentTab;
    private FormFactoryInterface $formFactory;
    private RequestStack $requestStack;
    private FormDataMapper $formDataMapper;
    private Repository $repository;
    private RouterInterface $router;
    private PermissionResolver $permissionResolver;
    private TabVisibilityManagerInterface $tabVisibilityManager;

    public function __construct(
        Environment $templating,
        ContentTab $contentTab,
        FormFactoryInterface $formFactory,
        RequestStack $requestStack,
        FormDataMapper $formDataMapper,
        Repository $repository,
        RouterInterface $router,
        PermissionResolver $permissionResolver,
        TabVisibilityManagerInterface $tabVisibilityManager
    ) {
        $this->templating = $templating;
        $this->contentTab = $contentTab;
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->formDataMapper = $formDataMapper;
        $this->repository = $repository;
        $this->router = $router;
        $this->permissionResolver = $permissionResolver;
        $this->tabVisibilityManager = $tabVisibilityManager;
    }

    /**
     * @param array<string, mixed> $parameters
     * @return bool
     * @throws BadStateException
     * @throws InvalidArgumentException
     */
    public function evaluate(array $parameters): bool
    {
        $contentType = $parameters['contentType'] ?? null;
        if (!$contentType instanceof ContentType) {
            return false;
        }

        $content = $parameters['content'] ?? null;
        if (!($content instanceof Content)) {
            return false;
        }

        if (!$this->tabVisibilityManager->isTabShouldBeDisplayed($contentType, $content)) {
            return false;
        }

        return $this->permissionResolver->canUser('seo', 'edit', $content);
    }

    public function getIdentifier(): string
    {
        return 'seo';
    }

    public function getName(): string
    {
        return 'SEO';
    }

    /**
     * @param array<string, mixed> $parameters
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderView(array $parameters): string
    {
        $content = $parameters['content'] ?? null;
        if (!($content instanceof Content)) {
            throw new DomainException('Unexpected content type');
        }
        $location = $parameters['location'] ?? null;
        if (!($location instanceof Location)) {
            throw new DomainException('Unexpected location type');
        }

        $currentRequest = $this->requestStack->getCurrentRequest();
        if ($currentRequest === null) {
            throw new DomainException('Unable to get current request');
        }

        $languageCode = $this->getCurrentLanguageCode($content, $currentRequest);
        $entityList = $this->repository->findByPropertyName($content, $languageCode);
        $formDataList = $this->formDataMapper->mapEntityListToFormDataList($entityList, $content, $languageCode);
        $form = $this->formFactory->create(ContentMetaForm::class, $formDataList, [
            'action' => $this->router->generate(
                'infostrates_ibexa_content_seo.update_manual_meta',
                ['contentId' => $content->id, 'languageCode' => $languageCode]
            ),
        ]);

        return $this->templating->render('@InfostratesIbexaContentSeo/tabs/location_view/seo.html.twig', [
            'content' => $content,
            'location' => $location,
            'languages' => $this->contentTab->loadContentLanguages($content),
            'form' => $form->createView(),
        ]);
    }

    private function getCurrentLanguageCode(Content $content, Request $currentRequest): string
    {
        if ($currentRequest->attributes->has('languageCode')) {
            $languageCode = $currentRequest->attributes->get('languageCode');
            if (!is_string($languageCode)) {
                throw new DomainException('Unexpected languageCode type from request');
            }

            return $languageCode;
        }

        $languageCode = $content->versionInfo->contentInfo->mainLanguageCode ?? null;
        if (!is_string($languageCode)) {
            throw new DomainException('Unexpected mainLanguageCode type from content');
        }

        return $languageCode;
    }
}
