<?php

namespace Infostrates\IbexaContentSeo\DependencyInjection\Compiler;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class DoctrineMappingsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $mappingPass = DoctrineOrmMappingsPass::createAnnotationMappingDriver(
            ['Infostrates\IbexaContentSeo\Domains'],
            [dirname(__DIR__, 2) . '/Domains']
        );
        $mappingPass->process($container);
    }
}
