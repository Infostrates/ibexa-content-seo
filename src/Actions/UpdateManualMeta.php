<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Actions;

use Ibexa\Contracts\Core\Repository\ContentService;
use Infostrates\IbexaContentSeo\Domains\Meta\ContentMetaForm;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormDataMapper;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Repository;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

final class UpdateManualMeta
{
    private ContentService $contentService;
    private Repository $repository;
    private FormDataMapper $formDataMapper;
    private FormFactoryInterface $formFactory;
    private RouterInterface $router;

    /**
     * @param ContentService       $contentService
     * @param Repository           $repository
     * @param FormDataMapper       $formDataMapper
     * @param FormFactoryInterface $formFactory
     * @param RouterInterface      $router
     */
    public function __construct(
        ContentService $contentService,
        Repository $repository,
        FormDataMapper $formDataMapper,
        FormFactoryInterface $formFactory,
        RouterInterface $router
    ) {
        $this->contentService = $contentService;
        $this->repository = $repository;
        $this->formDataMapper = $formDataMapper;
        $this->formFactory = $formFactory;
        $this->router = $router;
    }

    public function __invoke(Request $request, int $contentId, string $languageCode): RedirectResponse
    {
        $content = $this->contentService->loadContent($contentId);

        $entityByPropertyName = $this->repository->findByPropertyName($content, $languageCode);
        $formDataList = $this->formDataMapper->mapEntityListToFormDataList(
            $entityByPropertyName,
            $content,
            $languageCode
        );
        $form = $this->formFactory->create(ContentMetaForm::class, $formDataList);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formDataList = $form->getData();
            $newEntityList = $this->formDataMapper->mapFormDataListToEntityList(
                $formDataList,
                $content,
                $languageCode
            );
            $this->repository->replaceForContent($content, $languageCode, $newEntityList);
        }

        return new RedirectResponse($this->router->generate('ibexa.content.translation.view', [
            'contentId' => $content->id,
            'locationId' => $content->contentInfo->mainLocationId,
            'languageCode' => $languageCode,
        ]) . '#ez-tab-location-view-seo#tab');
    }
}
