<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo;

use Ibexa\Bundle\Core\DependencyInjection\IbexaCoreExtension;
use Infostrates\IbexaContentSeo\DependencyInjection\Compiler\DoctrineMappingsCompilerPass;
use Infostrates\IbexaContentSeo\Domains\PolicyProvider;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class InfostratesIbexaContentSeoBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);
        $container->addCompilerPass(new DoctrineMappingsCompilerPass());

        if ($container->hasExtension('ibexa')) {
            /** @var IbexaCoreExtension $ibexaExtension */
            $ibexaExtension = $container->getExtension('ibexa');
            $ibexaExtension->addPolicyProvider(new PolicyProvider());
        }
    }
}
