<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta;

use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Form;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormChoice;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\FormData;
use InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\FormBuilderInterface;

final class ContentMetaForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $dataArray = $builder->getData();
        if (!is_array($dataArray)) {
            throw new InvalidArgumentException('This form should not be created without data');
        }

        foreach ($dataArray as $propertyName => $data) {
            if (!$data instanceof FormData) {
                throw new InvalidArgumentException('Unexpected item type');
            }

            if (!empty($data->getChoices())) {
                $choices = new ArrayChoiceList($data->getChoices());
                $builder->add($propertyName, FormChoice::class, [
                    'choices' => $choices->getChoices(),
                    'required' => false,
                    'form_data' => $data,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]);
            } else {
                $builder->add($propertyName, Form::class, [
                    'required' => false,
                    'form_data' => $data,
                    'help' => $this->getHelpMessage($data)
                ]);
            }
        }
    }

    public function getHelpMessage(FormData $data): string
    {
        return sprintf(
            'If left empty, default value is : %s',
            $data->getAutomaticValue() ?: '<absent>'
        );
    }
}
