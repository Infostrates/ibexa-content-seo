<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta;

class MetaValue
{
    private string $propertyName;

    private string $value;

    public function __construct(string $propertyName, string $value)
    {
        $this->propertyName = $propertyName;
        $this->value = $value;
    }

    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
