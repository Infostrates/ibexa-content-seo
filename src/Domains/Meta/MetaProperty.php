<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta;

use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

final class MetaProperty
{
    private string $name;
    private ?AutoValueProviderInterface $autoValueProvider;
    /** @var string[] */
    private array $choices;

    /**
     * @param string $name
     * @param AutoValueProviderInterface|null $autoValueProvider
     * @param string[] $choices
     */
    public function __construct(string $name, ?AutoValueProviderInterface $autoValueProvider, array $choices)
    {
        $this->name = $name;
        $this->autoValueProvider = $autoValueProvider;
        $this->choices = $choices;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getAutoValueProvider(): ?AutoValueProviderInterface
    {
        return $this->autoValueProvider;
    }

    /**
     * @return string[]
     */
    public function getChoices(): array
    {
        return $this->choices;
    }
}
