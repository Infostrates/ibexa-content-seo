<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaProperty;

class Builder
{
    public function getAutomaticValue(
        ?Content $content,
        string $languageCode,
        MetaProperty $metaProperty,
        ?int $mainLocationId = null
    ): ?string {
        $autoValueProvider = $metaProperty->getAutoValueProvider();
        if (!$autoValueProvider) {
            return null;
        }

        return $autoValueProvider->buildAutoValue($content, $languageCode, $mainLocationId);
    }
}
