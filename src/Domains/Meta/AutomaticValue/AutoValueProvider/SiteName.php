<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

class SiteName implements AutoValueProviderInterface
{
    private string $siteName;

    public function __construct(string $siteName)
    {
        $this->siteName = $siteName;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        return $this->siteName;
    }
}
