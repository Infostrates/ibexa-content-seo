<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\Helper\TranslationHelper;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

class Description implements AutoValueProviderInterface
{
    private TranslationHelper $translationHelper;
    /** @var string[] */
    private array $possibleFieldIdentifierList;

    /**
     * @param TranslationHelper $translationHelper
     * @param string[]          $possibleFieldIdentifierList
     */
    public function __construct(TranslationHelper $translationHelper, array $possibleFieldIdentifierList)
    {
        $this->translationHelper = $translationHelper;
        $this->possibleFieldIdentifierList = $possibleFieldIdentifierList;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        if (!$content) {
            return null;
        }

        $descriptionField = null;
        foreach ($this->possibleFieldIdentifierList as $fieldIdentifier) {
            $field = $this->translationHelper->getTranslatedField($content, $fieldIdentifier, $languageCode);
            if ($field) {
                $descriptionField = $field;
                break;
            }
        }
        if (!$descriptionField) {
            return null;
        }

        $string = strip_tags((string)$descriptionField->value);

        return mb_substr($string, 0, 300);
    }
}
