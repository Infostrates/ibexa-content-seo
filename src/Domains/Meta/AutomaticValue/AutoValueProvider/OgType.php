<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

class OgType implements AutoValueProviderInterface
{
    /** @var array<string, string> */
    private array $contentTypeMapping;
    private string $defaultType;

    /**
     * @param string[] $contentTypeMapping
     * @param string   $defaultType
     */
    public function __construct(array $contentTypeMapping, string $defaultType)
    {
        $this->contentTypeMapping = $contentTypeMapping;
        $this->defaultType = $defaultType;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        if (!$content) {
            return null;
        }

        $contentTypeIdentifier = $content->getContentType()->identifier;

        return $this->contentTypeMapping[$contentTypeIdentifier] ?? $this->defaultType;
    }
}
