<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\Helper\TranslationHelper;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

class Title implements AutoValueProviderInterface
{
    private TranslationHelper $translationHelper;
    private string $siteName;
    /** @var string[] */
    private array $ignoreNameContentTypeList;

    /**
     * @param TranslationHelper $translationHelper
     * @param string            $siteName
     * @param string[]          $ignoreNameContentTypeList
     */
    public function __construct(
        TranslationHelper $translationHelper,
        string $siteName,
        array $ignoreNameContentTypeList
    ) {
        $this->translationHelper = $translationHelper;
        $this->siteName = $siteName;
        $this->ignoreNameContentTypeList = $ignoreNameContentTypeList;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        if (!$content || in_array($content->getContentType()->identifier, $this->ignoreNameContentTypeList, true)) {
            return $this->siteName;
        }

        return $this->translationHelper->getTranslatedContentName($content, $languageCode) . ' - ' . $this->siteName;
    }
}
