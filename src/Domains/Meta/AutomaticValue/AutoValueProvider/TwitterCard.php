<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;

class TwitterCard implements AutoValueProviderInterface
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        return $this->value;
    }
}
