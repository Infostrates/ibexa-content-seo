<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\Helper\FieldHelper;
use Ibexa\Core\Helper\TranslationHelper;
use Ibexa\Contracts\Core\Variation\Values\Variation;
use Ibexa\Contracts\Core\Variation\VariationHandler;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;
use Symfony\Component\Asset\PackageInterface;

class Image implements AutoValueProviderInterface
{
    private TranslationHelper $translationHelper;
    private FieldHelper $fieldHelper;
    private VariationHandler $variationHandler;
    private PackageInterface $package;
    /** @var string[] */
    private array $possibleFieldIdentifierList;
    private string $imageVariation;

    /**
     * @param TranslationHelper $translationHelper
     * @param FieldHelper       $fieldHelper
     * @param VariationHandler  $variationHandler
     * @param PackageInterface  $package
     * @param string[]          $possibleFieldIdentifierList
     * @param string            $imageVariation
     */
    public function __construct(
        TranslationHelper $translationHelper,
        FieldHelper $fieldHelper,
        VariationHandler $variationHandler,
        PackageInterface $package,
        array $possibleFieldIdentifierList,
        string $imageVariation
    ) {
        $this->translationHelper = $translationHelper;
        $this->fieldHelper = $fieldHelper;
        $this->variationHandler = $variationHandler;
        $this->package = $package;
        $this->possibleFieldIdentifierList = $possibleFieldIdentifierList;
        $this->imageVariation = $imageVariation;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        if (!$content) {
            return null;
        }

        $imageField = null;
        foreach ($this->possibleFieldIdentifierList as $fieldIdentifier) {
            $field = $this->translationHelper->getTranslatedField($content, $fieldIdentifier, $languageCode);
            if ($field) {
                $imageField = $field;
                break;
            }
        }
        if (!$imageField) {
            return null;
        }

        if ($this->fieldHelper->isFieldEmpty($content, $imageField->fieldDefIdentifier, $languageCode)) {
            return null;
        }

        $variation = $this->variationHandler->getVariation(
            $imageField,
            $content->versionInfo,
            $this->imageVariation
        );
        if (!$variation instanceof Variation) {
            return null;
        }

        return $this->package->getUrl($variation->uri);
    }
}
