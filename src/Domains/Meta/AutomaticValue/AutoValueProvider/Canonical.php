<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProvider;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\Helper\TranslationHelper;
use Ibexa\Core\MVC\Symfony\Routing\UrlAliasRouter;
use Ibexa\Core\MVC\Symfony\SiteAccess;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Canonical implements AutoValueProviderInterface
{
    private RouterInterface $router;
    private RequestStack $requestStack;
    private TranslationHelper $translationHelper;

    public function __construct(
        RouterInterface $router,
        RequestStack $requestStack,
        TranslationHelper $translationHelper
    ) {
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->translationHelper = $translationHelper;
    }

    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string
    {
        if (!$content) {
            return null;
        }
        $mainLocationId = $mainLocationId ?? $content->contentInfo->mainLocationId;
        if (!$mainLocationId) {
            return null;
        }

        $routeParameters = ['locationId' => $mainLocationId];
        if (
            ($mainRequest = $this->requestStack->getMainRequest())
            && ($siteaccess = $mainRequest->attributes->get('siteaccess'))
            && $siteaccess instanceof SiteAccess
        ) {
            $siteAccessName = $siteaccess->name;
            if ($siteAccessName === 'admin') {
                $siteAccessName = $this->translationHelper->getTranslationSiteAccess($languageCode) ?? $siteAccessName;
            }
            $routeParameters['siteaccess'] = $siteAccessName;
        }

        return $this->router->generate(
            UrlAliasRouter::URL_ALIAS_ROUTE_NAME,
            $routeParameters,
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}
