<?php

namespace Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;

interface AutoValueProviderInterface
{
    public function buildAutoValue(?Content $content, string $languageCode, ?int $mainLocationId): ?string;
}
