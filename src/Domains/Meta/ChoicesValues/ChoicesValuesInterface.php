<?php

namespace Infostrates\IbexaContentSeo\Domains\Meta\ChoicesValues;

interface ChoicesValuesInterface
{
    /**
     * @return string[]
     */
    public function getValue(): array;
}
