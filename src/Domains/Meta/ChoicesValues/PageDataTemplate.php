<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ChoicesValues;

class PageDataTemplate implements ChoicesValuesInterface
{
    /** @var string[]  */
    private array $value;

    /**
     * @param string[] $value
     */
    public function __construct(array $value)
    {
        $this->value = $value;
    }

    /**
     * @return string[]
     */
    public function getValue(): array
    {
        return $this->value;
    }
}
