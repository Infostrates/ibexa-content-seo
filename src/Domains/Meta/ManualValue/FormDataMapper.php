<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\Builder;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaPropertyRegistry;

final class FormDataMapper
{
    private MetaPropertyRegistry $metaPropertyRegistry;
    private Builder $automaticValueBuilder;

    public function __construct(MetaPropertyRegistry $metaPropertyRegistry, Builder $automaticValueBuilder)
    {
        $this->metaPropertyRegistry = $metaPropertyRegistry;
        $this->automaticValueBuilder = $automaticValueBuilder;
    }

    /**
     * @param FormData[]   $formDataList
     * @param Content $content
     * @param string  $languageCode
     * @return array<int, Entity>
     */
    public function mapFormDataListToEntityList(array $formDataList, Content $content, string $languageCode): array
    {
        $entityList = [];
        foreach ($formDataList as $formData) {
            $entity = $formData->createEntity($content, $languageCode);
            if ($entity) {
                $entityList[] = $entity;
            }
        }

        return $entityList;
    }

    /**
     * @param array<string, Entity> $entityByPropertyName
     * @param Content               $content
     * @param string                $languageCode
     * @return array<string, FormData>
     */
    public function mapEntityListToFormDataList(
        array $entityByPropertyName,
        Content $content,
        string $languageCode
    ): array {
        $formDataList = [];
        foreach ($this->metaPropertyRegistry->getAll() as $property) {
            $name = $property->getName();
            $formDataList[$name] = FormData::createFromEntity(
                $entityByPropertyName[$name] ?? null,
                $property,
                $this->automaticValueBuilder->getAutomaticValue($content, $languageCode, $property),
                $property->getChoices()
            );
        }

        return $formDataList;
    }
}
