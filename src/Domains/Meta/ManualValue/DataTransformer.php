<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use DomainException;
use Symfony\Component\Form\DataTransformerInterface;

final class DataTransformer implements DataTransformerInterface
{
    private FormData $data;

    public function __construct(FormData $data)
    {
        $this->data = $data;
    }

    /**
     * @param mixed $value
     * @return FormData
     */
    public function reverseTransform($value): FormData
    {
        if (!is_string($value) && !is_null($value)) {
            throw new DomainException('Unexpected value type');
        }

        $this->data->value = $value ?: null;

        return $this->data;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function transform($value): string
    {
        if (!$value) {
            return '';
        }

        if (!$value instanceof FormData) {
            throw new DomainException('Unexpected value type');
        }

        return (string)$value->value;
    }
}
