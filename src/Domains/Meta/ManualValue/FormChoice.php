<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;

final class FormChoice extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['form_data'] instanceof FormData) {
            throw new InvalidArgumentException('form_data option should be set');
        }

        $builder->resetViewTransformers();
        $builder->addViewTransformer(
            new ChoiceTransformer(
                new ArrayChoiceList($options['form_data']->getChoices()),
                $options['form_data']
            )
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'choices' => [],
            'compound' => false,
            'label_format' => '%name%',
            'translation_domain' => false,
        ]);
        $resolver->setRequired(['form_data']);
        $resolver->setAllowedTypes('form_data', FormData::class);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
