<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaProperty;

final class FormData
{
    private string $propertyName;
    public ?string $value;
    private ?string $automaticValue;
    /** @var string[] */
    private array $choices;

    /**
     * @param string $propertyName
     * @param string|null $value
     * @param string|null $automaticValue
     * @param string[] $choices
     */
    public function __construct(string $propertyName, ?string $value, ?string $automaticValue, array $choices = [])
    {
        $this->propertyName = $propertyName;
        $this->value = $value;
        $this->automaticValue = $automaticValue;
        $this->choices = $choices;
    }

    /**
     * @param Entity|null $entity
     * @param MetaProperty $property
     * @param string|null $automaticValue
     * @param string[] $choices
     * @return FormData
     */
    public static function createFromEntity(
        ?Entity $entity,
        MetaProperty $property,
        ?string $automaticValue,
        array $choices
    ): FormData {
        return new self(
            $property->getName(),
            $entity->value ?? null,
            $automaticValue,
            $choices
        );
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    public function getAutomaticValue(): ?string
    {
        return $this->automaticValue;
    }

    /**
     * @return string[]
     */
    public function getChoices(): array
    {
        return $this->choices;
    }

    public function createEntity(Content $content, string $languageCode): ?Entity
    {
        if ($this->value === null) {
            return null;
        }

        $entity = new Entity($content->id, $languageCode, $this->propertyName);
        $entity->value = $this->value;

        return $entity;
    }
}
