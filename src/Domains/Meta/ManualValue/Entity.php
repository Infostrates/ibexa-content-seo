<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(
 *     name="ismeta_manual_value",
 *     indexes={@ORM\Index(name="content_id_language_code_idx", columns={"content_id", "language_code"})}
 * )
 */
class Entity
{
    /**
     * @ORM\Column(type="integer", name="content_id")
     * @ORM\Id()
     */
    private int $contentId;

    /**
     * @ORM\Column(type="string", length=6, name="language_code")
     * @ORM\Id()
     */
    private string $languageCode;

    /**
     * @ORM\Column(type="string", name="property_name")
     * @ORM\Id()
     */
    private string $propertyName;

    /**
     * @ORM\Column(type="string", name="value")
     */
    public string $value;

    /**
     * @param int    $contentId
     * @param string $languageCode
     * @param string $propertyName
     */
    public function __construct(int $contentId, string $languageCode, string $propertyName)
    {
        $this->contentId = $contentId;
        $this->languageCode = $languageCode;
        $this->propertyName = $propertyName;
    }

    public function getContentId(): int
    {
        return $this->contentId;
    }

    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    public function getPropertyName(): string
    {
        return $this->propertyName;
    }
}
