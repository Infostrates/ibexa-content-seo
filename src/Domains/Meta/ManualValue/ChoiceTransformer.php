<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use DomainException;
use Symfony\Component\Form\ChoiceList\ArrayChoiceList;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

final class ChoiceTransformer implements DataTransformerInterface
{
    private ArrayChoiceList $choiceList;
    private FormData $data;

    public function __construct(ArrayChoiceList $choiceList, FormData $data)
    {
        $this->choiceList = $choiceList;
        $this->data = $data;
    }

    public function transform($value): string
    {
        if (!$value) {
            return '';
        }

        if (!$value instanceof FormData) {
            throw new DomainException('Unexpected value type');
        }

        return (string) current($this->choiceList->getValuesForChoices([$value->value]));
    }

    public function reverseTransform($value): ?FormData
    {
        if (!is_string($value) && !is_null($value)) {
            throw new DomainException('Unexpected value type');
        }

        if (!empty($value)) {
            $choices = $this->choiceList->getChoicesForValues([(string) $value]);

            if (1 !== \count($choices)) {
                throw new TransformationFailedException(
                    sprintf('The choice "%s" does not exist or is not unique.', $value)
                );
            }
        }

        $this->data->value = $value ?: null;

        return $this->data;
    }
}
