<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['form_data'] instanceof FormData) {
            throw new InvalidArgumentException('form_data option should be set');
        }

        $builder->addViewTransformer(new DataTransformer($options['form_data']));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'compound' => false,
            'label_format' => '%name%',
            'translation_domain' => false,
        ]);
        $resolver->setRequired(['form_data']);
        $resolver->setAllowedTypes('form_data', FormData::class);
    }

    public function getParent(): string
    {
        return TextType::class;
    }
}
