<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta\ManualValue;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use DomainException;
use Exception;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;

/**
 * @extends ServiceEntityRepository<Entity>
 */
class Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

    /**
     * @param Content $content
     * @param string  $languageCode
     * @return array<string, Entity>
     */
    public function findByPropertyName(Content $content, string $languageCode, ?int $mainContentId = null): array
    {
        /** @var Entity[] $entityList */
        $entityList = $this->findBy(['contentId' => $mainContentId ?? $content->id, 'languageCode' => $languageCode]);

        $entityByPropertyName = [];
        foreach ($entityList as $entity) {
            $entityByPropertyName[$entity->getPropertyName()] = $entity;
        }

        return $entityByPropertyName;
    }

    /**
     * @param Content  $content
     * @param string   $languageCode
     * @param Entity[] $newEntityList
     * @return void
     */
    public function replaceForContent(Content $content, string $languageCode, array $newEntityList): void
    {
        try {
            $currentList = $this->findByPropertyName($content, $languageCode);
            $currentListByIdentifiers = [];
            foreach ($currentList as $entity) {
                $currentListByIdentifiers[$this->getEntityIdentifiers($entity)] = $entity;
            }
            $updatedIdentifiersList = [];
            foreach ($newEntityList as $newEntity) {
                $newEntityIdentifiers = $this->getEntityIdentifiers($newEntity);
                if (isset($currentListByIdentifiers[$newEntityIdentifiers])) {
                    //update
                    $currentListByIdentifiers[$newEntityIdentifiers]->value = $newEntity->value;
                    $updatedIdentifiersList[] = $newEntityIdentifiers;
                } else {
                    //creation
                    $this->getEntityManager()->persist($newEntity);
                }
            }// remove non updated entities
            $entityToRemoveList = array_diff_key($currentListByIdentifiers, array_flip($updatedIdentifiersList));
            foreach ($entityToRemoveList as $entityToRemove) {
                $this->getEntityManager()->remove($entityToRemove);
            }
            $this->getEntityManager()->flush();
        } catch (Exception $e) {
            throw new DomainException(
                'Unable to replace manual meta entity for content ' . $content->id . ' and language ' . $languageCode,
                0,
                $e
            );
        }
    }

    private function getEntityIdentifiers(Entity $entity): string
    {
        return $entity->getContentId() . '-' . $entity->getLanguageCode() . '-' . $entity->getPropertyName();
    }
}
