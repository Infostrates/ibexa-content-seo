<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\Builder;
use Infostrates\IbexaContentSeo\Domains\Meta\ManualValue\Repository;

class MetaValuesProvider
{
    private Repository $manualMetaRepository;
    private MetaPropertyRegistry $propertyRegistry;
    private Builder $automaticValueBuilder;

    public function __construct(
        Repository $manualMetaRepository,
        MetaPropertyRegistry $propertyRegistry,
        AutomaticValue\Builder $automaticValueBuilder
    ) {
        $this->manualMetaRepository = $manualMetaRepository;
        $this->propertyRegistry = $propertyRegistry;
        $this->automaticValueBuilder = $automaticValueBuilder;
    }

    /**
     * @param Content|null             $content
     * @param string                   $languageCode
     * @param array<string, MetaValue> $forcedMetaValueByName
     * @param int|null                 $mainLocationId
     * @return array<string, MetaValue>
     */
    public function buildMetaValuesByName(
        ?Content $content,
        string $languageCode,
        array $forcedMetaValueByName = [],
        ?int $mainLocationId = null,
        ?int $mainContentId = null
    ): array {
        $entityByPropertyName = $content
            ? $this->manualMetaRepository->findByPropertyName($content, $languageCode, $mainContentId)
            : []
        ;

        $propertyList = $this->propertyRegistry->getAll();
        $metaValueByName = [];
        foreach ($propertyList as $property) {
            $propertyName = $property->getName();
            $entity = $entityByPropertyName[$propertyName] ?? null;
            if ($entity) {
                $metaValueByName[$propertyName] = new MetaValue($propertyName, $entity->value);
                continue;
            }

            $forcedMetaValue = $forcedMetaValueByName[$propertyName] ?? null;
            if ($forcedMetaValue) {
                $metaValueByName[$propertyName] = $forcedMetaValue;
                continue;
            }

            $autoValue = $this->automaticValueBuilder->getAutomaticValue(
                $content,
                $languageCode,
                $property,
                $mainLocationId
            );
            if ($autoValue !== null) {
                $metaValueByName[$propertyName] = new MetaValue($propertyName, $autoValue);
            }
        }

        $forcedPropertyNameList = array_diff(array_keys($forcedMetaValueByName), array_map(
            static fn (MetaProperty $property) => $property->getName(),
            $propertyList
        ));
        foreach ($forcedPropertyNameList as $forcedPropertyName) {
            $metaValueByName[$forcedPropertyName] = $forcedMetaValueByName[$forcedPropertyName];
        }

        return $metaValueByName;
    }
}
