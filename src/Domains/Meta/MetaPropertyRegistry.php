<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\Meta;

use DomainException;
use Infostrates\IbexaContentSeo\Domains\Meta\AutomaticValue\AutoValueProviderInterface;
use Infostrates\IbexaContentSeo\Domains\Meta\ChoicesValues\ChoicesValuesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class MetaPropertyRegistry
{
    /** @var MetaProperty[] */
    private array $registry;

    /**
     * @param array<int, array<string, mixed>> $config
     * @param ContainerInterface               $container
     */
    public function __construct(array $config, ContainerInterface $container)
    {
        // TODO validate config schema using the config component
        $this->registry = [];
        foreach ($config as $configItem) {
            $propertyName = $configItem['name'];
            if (!$propertyName) {
                throw new DomainException('Missing name attribute for meta property');
            }
            $choicesServiceIdentifier = $configItem['choices'] ?? null;
            /** @var ChoicesValuesInterface|null  $choices */
            $choices = $choicesServiceIdentifier
                ? $container->get($choicesServiceIdentifier)
                : null
            ;
            $choicesValue = $choices ? $choices->getValue() : [];
            $autoValueProviderServiceIdentifier = $configItem['auto_value_provider'] ?? null;
            $autoValueProvider = $autoValueProviderServiceIdentifier
                ? $container->get($autoValueProviderServiceIdentifier)
                : null
            ;
            if (!$autoValueProvider) {
                $this->registry[] = new MetaProperty($propertyName, null, $choicesValue);
                continue;
            }

            if (!$autoValueProvider instanceof AutoValueProviderInterface) {
                throw new DomainException(
                    "auto_value_provider service for meta property $propertyName " .
                    "does not extend " . AutoValueProviderInterface::class
                );
            }
            $this->registry[] = new MetaProperty(
                $propertyName,
                $autoValueProvider,
                $choicesValue
            );
        }
    }

    /**
     * @return MetaProperty[]
     */
    public function getAll(): array
    {
        return $this->registry;
    }
}
