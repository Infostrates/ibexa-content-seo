<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;
use Infostrates\IbexaContentUtils\PageContentTypeGuesser;

class TabVisibilityManager implements TabVisibilityManagerInterface
{
    protected ?PageContentTypeGuesser $pageContentTypeGuesser;

    /**
     * @param PageContentTypeGuesser|null $pageContentTypeGuesser
     */
    public function __construct(?PageContentTypeGuesser $pageContentTypeGuesser)
    {
        $this->pageContentTypeGuesser = $pageContentTypeGuesser;
    }

    public function isTabShouldBeDisplayed(ContentType $contentType, Content $content): bool
    {
        if ($this->pageContentTypeGuesser) {
            $isPageContent = $this->pageContentTypeGuesser->hasContentTypeHaveContentPage(
                $contentType,
                $this->getTargetSiteAccess()
            );
            if (!$isPageContent) {
                return false;
            }
        }

        return true;
    }


    protected function getTargetSiteAccess(): string
    {
        return 'site_fr';
    }
}
