<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\HrefLang;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Core\Helper\TranslationHelper;
use Ibexa\Core\MVC\Symfony\Routing\UrlAliasRouter;
use Ibexa\AdminUi\UI\Dataset\DatasetFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class Provider
{
    private const X_DEFAULT_LOCALE = 'en';

    private TranslationHelper $translationHelper;
    private DatasetFactory $datasetFactory;
    private RouterInterface $router;

    /**
     * @param TranslationHelper $translationHelper
     * @param DatasetFactory    $datasetFactory
     * @param RouterInterface   $router
     */
    public function __construct(
        TranslationHelper $translationHelper,
        DatasetFactory $datasetFactory,
        RouterInterface $router
    ) {
        $this->translationHelper = $translationHelper;
        $this->datasetFactory = $datasetFactory;
        $this->router = $router;
    }

    /**
     * @param Content $content
     * @param int|null $mainContentId
     * @return HrefLang[]
     */
    public function buildHrefLangList(Content $content, ?int $mainContentId): array
    {
        if ($content->contentInfo->alwaysAvailable) {
            $availableLanguageCodeList = $this->translationHelper->getAvailableLanguages();
        } else {
            $translationsDataset = $this->datasetFactory->translations();
            $translationsDataset->load($content->versionInfo);

            $availableLanguageCodeList = $translationsDataset->getLanguageCodes();
        }

        if (count($availableLanguageCodeList) < 2) {
            return [];
        }

        $hrefLangByLocaleCode = [];
        foreach ($availableLanguageCodeList as $languageCode) {
            $localeCode = $this->getLocaleCodeByLanguageCode($languageCode);
            $hrefLangByLocaleCode[$localeCode] = new HrefLang(
                $localeCode,
                $this->router->generate(
                    UrlAliasRouter::URL_ALIAS_ROUTE_NAME,
                    [
                        'contentId' => $mainContentId ?? $content->id,
                        'siteaccess' => $this->translationHelper->getTranslationSiteAccess($languageCode),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            );
        }

        if (isset($hrefLangByLocaleCode[self::X_DEFAULT_LOCALE])) {
            $hrefLangByLocaleCode['x-default'] = new HrefLang(
                'x-default',
                $hrefLangByLocaleCode[self::X_DEFAULT_LOCALE]->getHref()
            );
        }

        return array_values($hrefLangByLocaleCode);
    }

    private function getLocaleCodeByLanguageCode(string $languageCode): string
    {
        $languageCode = strtolower($languageCode);

        if (preg_match('/^[a-z]{3}(-[a-z]{2})?$/', $languageCode)) {
            $iso639_2 = substr($languageCode, 0, 3);
            $iso639Mapping = $this->getMappingIso6392To6391LanguageCode();
            return $iso639Mapping[$iso639_2] ?? substr($iso639_2, 0, 2);
        }

        if (preg_match('/^[a-z]{2}([-_][a-z]{2})$/', $languageCode)) {
            return substr($languageCode, 0, 2);
        }

        if (preg_match('/^[a-z]{2}$/', $languageCode)) {
            return $languageCode;
        }

        return substr($languageCode, 0, 2);
    }

    /**
     * @return string[]
     */
    private function getMappingIso6392To6391LanguageCode(): array
    {
        return [
            "eng" => "en",
            "fra" => "fr",
            "fre" => "fr",
            "deu" => "de",
            "ger" => "de",
            "spa" => "es",
            "ita" => "it",
            "por" => "pt",
            "rus" => "ru",
            "zho" => "zh",
            "ara" => "ar",
            "jpn" => "ja",
            "kor" => "ko",
            "hin" => "hi",
            "nld" => "nl",
            "dut" => "nl",
            "swe" => "sv",
            "nor" => "no",
            "dan" => "da",
            "fin" => "fi",
            "pol" => "pl",
            "ukr" => "uk",
            "ces" => "cs",
            "cze" => "cs",
            "hun" => "hu",
            "ell" => "el",
            "gre" => "el",
            "bul" => "bg",
            "ron" => "ro",
            "rum" => "ro",
            "slv" => "sl",
            "hrv" => "hr",
            "srp" => "sr",
            "tur" => "tr",
            "tha" => "th",
            "vie" => "vi",
            "tam" => "ta",
            "urd" => "ur",
            "ben" => "bn",
            "mal" => "ml",
            "tel" => "te",
            "mar" => "mr",
            "guj" => "gu",
            "pan" => "pa",
            "tgl" => "tl",
            "msa" => "ms",
            "may" => "ms",
            "ind" => "id",
            "swa" => "sw",
            "amh" => "am",
            "hau" => "ha",
            "yor" => "yo",
            "ibo" => "ig",
            "zul" => "zu",
        ];
    }
}
