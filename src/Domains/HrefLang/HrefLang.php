<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains\HrefLang;

class HrefLang
{
    private string $localeCode;
    private string $href;

    public function __construct(string $localeCode, string $href)
    {
        $this->localeCode = $localeCode;
        $this->href = $href;
    }

    public function getLocaleCode(): string
    {
        return $this->localeCode;
    }

    public function getHref(): string
    {
        return $this->href;
    }
}
