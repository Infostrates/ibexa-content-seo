<?php

declare(strict_types=1);

namespace Infostrates\IbexaContentSeo\Domains;

use DomainException;
use Ibexa\Contracts\Core\Repository\LanguageResolver;
use Ibexa\Contracts\Core\Repository\LocationService;
use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Infostrates\IbexaContentSeo\Domains\HrefLang\Provider;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaValue;
use Infostrates\IbexaContentSeo\Domains\Meta\MetaValuesProvider;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    private MetaValuesProvider $metaValuesProvider;
    private Environment $templating;
    private LanguageResolver $languageResolver;
    private ?Provider $hrefLangProvider;
    private LocationService $locationService;

    public function __construct(
        MetaValuesProvider $metaValuesProvider,
        Environment $templating,
        LanguageResolver $languageResolver,
        ?HrefLang\Provider $hrefLangProvider,
        LocationService $locationService
    ) {
        $this->metaValuesProvider = $metaValuesProvider;
        $this->templating = $templating;
        $this->languageResolver = $languageResolver;
        $this->hrefLangProvider = $hrefLangProvider;
        $this->locationService = $locationService;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('seo_metas', [$this, 'getSeoMetasHtml'], ['needs_context' => true, 'is_safe' => ['html']]),
            new TwigFunction('get_seo_meta', [$this, 'getSeoMeta'], ['needs_context' => true, 'is_safe' => ['html']]),
            new TwigFunction('get_forced_all_seo_meta', [$this, 'getForcedAllSeoMeta'], [
                'needs_context' => false,
                'is_safe' => ['html']
            ]),
        ];
    }

    /**
     * @param array<string, mixed> $context
     * @return string
     */
    public function getSeoMetasHtml(array $context): string
    {
        $metaValueByName = $this->getMetaValueByName($context);

        if (empty($metaValueByName)) {
            return '';
        }

        $content = $context['content'] ?? null;
        $mainContentId = $context['forced_content_id'] ?? null;

        return $this->templating->render('@InfostratesIbexaContentSeo/seo_metas.html.twig', [
            'meta_value_by_name' => $metaValueByName,
            'href_lang_list' => ($this->hrefLangProvider && $content)
                ? $this->hrefLangProvider->buildHrefLangList($content, $mainContentId)
                : [],
        ]);
    }

    /**
     * @param array<string, mixed> $context
     * @return string
     */
    public function getSeoMeta(array $context, string $name): ?string
    {
        $metaValueByName = $this->getMetaValueByName($context);

        if (empty($metaValueByName)) {
            return null;
        }

        return array_key_exists($name, $metaValueByName) ? $metaValueByName[$name]->getValue() : null;
    }

    /**
     * @param Content $content
     * @return array<string>
     */
    public function getForcedAllSeoMeta(Content $content): array
    {
        $contentMetaValueList = $this->getMetaValueByName(['content' => $content]);
        return array_map(function (MetaValue $metaValue) {
            return $metaValue->getValue();
        }, $contentMetaValueList);
    }

    /**
     * @param array<string, mixed> $context
     * @return array<string, MetaValue>
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    private function getMetaValueByName(array $context): array
    {
        $content = $context['content'] ?? null;
        $listLocations = $content ? $this->locationService->loadLocations($content->contentInfo) : [];
        $location = $context['location'] ?? null;
        $mainContentId = $context['forced_content_id'] ?? null;
        $mainLocationId = $context['forced_location_id'] ?? ($location && $listLocations > 1 ? $location->id : null);

        $languageCode = $this->languageResolver->getPrioritizedLanguages()[0] ?? null;
        if (!$languageCode) {
            return []; //Unable to get current language code : no metas
        }

        $forcedMetaValueByName = $this->getForcedMetaValueByNameFromContext($context['forced_metas'] ?? null);

        return $this->metaValuesProvider->buildMetaValuesByName(
            $content,
            $languageCode,
            $forcedMetaValueByName,
            $mainLocationId,
            $mainContentId
        );
    }

    /**
     * @param mixed $forcedMetaHash
     * @return array<string, MetaValue>
     */
    private function getForcedMetaValueByNameFromContext($forcedMetaHash): array
    {
        if (!is_array($forcedMetaHash)) {
            $forcedMetaHash = [];
        }
        $forcedMetaValueByName = [];
        foreach ($forcedMetaHash as $name => $value) {
            $forcedMetaValueByName[$name] = new MetaValue($name, $value);
        }
        return $forcedMetaValueByName;
    }
}
