<?php

namespace Infostrates\IbexaContentSeo\Domains;

use Ibexa\Contracts\Core\Repository\Values\Content\Content;
use Ibexa\Contracts\Core\Repository\Values\ContentType\ContentType;

interface TabVisibilityManagerInterface
{
    public function isTabShouldBeDisplayed(ContentType $contentType, Content $content): bool;
}
